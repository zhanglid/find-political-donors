# Introduction
This is a project for the assignment of insight data engineer fellowship. The aim is to help identify possible donors for a variety of upcoming election campaigns. We want to identify the areas (zip codes) that may be fertile ground for soliciting future donations for similar candidates. 

# Summary
For this challenge, I take an input file that lists campaign contributions by individual donors and distill it into two output files.
1. `medianvals_by_zip.txt`: contains a calculated running median, total dollar amount and total number of contributions by recipient and zip code
2. `medianvals_by_date.txt`: has the calculated median, total dollar amount and total number of contributions by recipient and date.

# Usage
The project is written in `python 2.7`. You can simplely run `run.sh`.
```bash
./run.sh
``` 
Or use the following command to specify input and output tiles
```bash
python ./src/find_political_donors.py ./input/itcont.txt ./output/medianvals_by_zip.txt ./output/medianvals_by_date.txt
```

If you want to run the unit test in `my_unit_test` folder, you have to install pytest.
```bash
pip install pytest==3.2.1
```
And run pytest
```bash
pytest
```

# Algorithm
The first step is to parse relevant data from file. Since the data is confirmed to follow the FEC header, we splits each line with delimer and extract the data from their positions. 

The second step is to put the data into their group. I use a tuple as the key for a dict to store the data. In order to find the median quickly in stream, I use two heaps to store each half of the data.  

The third step is to output files. For the `medianvals_by_zip.txt`, as the requirement that calculating the running median, I output the file when it reads and processes each line. Because `medianvals_by_date.txt` is irrelevant to order, I outpur this file when it finishs reading and processsing input file.

# Repo directory structure
```
.
├── README.md
├── input
│   └── itcont.txt
├── insight_testsuite
│   ├── results.txt
│   ├── run_tests.sh
│   └── tests
│       └── test_1
│           ├── README.md
│           ├── input
│           │   └── itcont.txt
│           └── output
│               ├── medianvals_by_date.txt
│               └── medianvals_by_zip.txt
├── my_unit_test
│   ├── __init__.py
│   ├── test_median_find.py
│   └── test_political_donors_finder.py
├── output
├── run.sh
└── src
    ├── __init__.py
    └── find_political_donors.py
```
