# -*- coding: utf-8 -*-
"""The script calculate the running median for FEC Contributions by Individuals dataset.

The script uses the two heaps algorithm to get the running median. We split the data
belong to one unique part into two parts with mostly equal size and use the max of the
smaller part and min of the larger part to get the median.

input file: pipe-delimited txt file followed FEC header
CMTE_ID,AMNDT_IND,RPT_TP,TRANSACTION_PGI,IMAGE_NUM,TRANSACTION_TP,ENTITY_TP,NAME,CITY,STATE,
ZIP_CODE,EMPLOYER,OCCUPATION,TRANSACTION_DT,TRANSACTION_AMT,OTHER_ID,TRAN_ID,FILE_NUM,MEMO_CD,MEMO_TEXT,SUB_ID

output files:
1. "medianvals_by_zip.txt": The running median grouped by cmte_id-zip_code following the order of input.
CMTE_ID|ZIP_CODE_5DIGIT|MEDIAN|COUNT|TOTAL_AMOUNT

2. "medianvals_by_zip.txt": The median grouped by cmte_id-date sorted by CMTE_ID alphabetically and then
DATE chronologically
CMTE_ID|DATE|MEDIAN|COUNT|TOTAL_AMOUNT

usage:
python find_political_donors.py path/to/input/file.txt path/to/medianvals_by_zip.txt path/to/medianvals_by_date.txt

"""

import sys
from heapq import *
import datetime

__author__ = "Zhangliang Dong"
__license__ = "GPL"
__email__ = "dongzl2017@gmail.com"

INPUT_DELIMER = "|"
OUTPUT_DELIMER = "|"


def add_amount(small, large, amount):
    """ Two heaps algorithm to find the stream median with complexity of O(nlogn).

    small is a max heap used to store smaller half of the data. large in a min heap
    used to store larger half of the data. Each time we maintain the size of the small
    heap is equal to size of large heap or at most 1 bigger.

    :param small: the min heap
    :param large: the max heap
    :param amount: the element to be inserted
    """
    if len(small) > len(large):
        heappush(large, -heappushpop(small, -amount))
    else:
        heappush(small, -heappushpop(large, amount))


def get_median(small, large):
    """Get the median by two heaps algorithm

    If there are even elements, the median is the average value of tops of two heaps.
    If there are odd elements, the median is the top of small heap, since we maintain
    the size of small heap is always greater or equal than large heap.

    :param small: the min heap
    :param large: the max heap
    :return: rounded median
    """
    if (len(small) + len(large)) % 2 == 0:
        return int(round((large[0] - small[0]) / 2.0))
    else:
        return -small[0]


class PoliticalDonorsMedianFinder:
    """The finder takes input data store it and get the running zip data and generate the date report """
    def __init__(self):
        """Initialize the class

        Create two dict to store unique grouped values

        id_date_dict: { (CMTE_ID, DATETIME): [COUNT, AMOUNT, SMALL_HEAP, LARGE_HEAP, DATE] }
        zip_dict: { (ZIP_CODE, CMTE_ID): [COUNT, AMOUNT, SMALL_HEAP, LARGE_HEAP] }

        """
        self.id_date_dict = {}
        self.zip_dict = {}

    def put_and_get_zip_status(self, cmte_id, zip_code, date_str, amount, other_id):
        """The function used put new data into the dict

        The function uses the relevant data CMTE_ID, ZIP_CODE, TRANSACTION_DT, TRANSACTION_AMT, OTHER_ID
        to update two dicts. And get the cmte_id-zip grouped data back.

        :param cmte_id: CMTE_ID
        :param zip_code: ZIP_CODE
        :param date_str: TRANSACTION_DT
        :param amount: TRANSACTION_AMT
        :param other_id: OTHER_ID
        :return: a tuple contains cmte_id-zip combined data: (CMTE_ID, ZIP_CODE, MEDAIN, COUNT, TOTAL_AMOUNT)
                If the input is invalid or it is invalid for cmte_id-zip grouped data, it will return None.
        """

        # ignore personal donate
        if other_id:
            return None

        # ignore empty cmte_id
        if not cmte_id:
            return None

        # ignore invalid amount
        try:
            amount = int(amount)
        except ValueError:
            return None

        # only consider 5 digits zip code
        if len(zip_code) > 5:
            zip_code = zip_code[:5]
        res = None

        if len(zip_code) == 5:

            # update zip data
            key = (zip_code, cmte_id)
            if key not in self.zip_dict:
                self.zip_dict[key] = (0, 0, [], [])

            old_count, old_amount, small, large = self.zip_dict[key]
            add_amount(small, large, amount)
            total_count = old_count + 1
            total_amount = old_amount + amount
            self.zip_dict[key] = (total_count, total_amount, small, large)

            # generate the cmte_id-zip combined data
            res = (cmte_id, zip_code, str(get_median(small, large)), str(total_count), str(total_amount))

        # ignore for cmte_id-date combined data when date is invalid
        try:
            date = datetime.datetime.strptime(date_str, '%m%d%Y')
        except ValueError:
            return res

        # update cmte_id-date combined data
        key = (cmte_id, date)

        if key not in self.id_date_dict:
            self.id_date_dict[key] = (0, 0, [], [], date_str)

        old_count, old_amount, small, large, _ = self.id_date_dict[key]
        add_amount(small, large, amount)
        self.id_date_dict[key] = (old_count + 1, old_amount + amount, small, large, date_str)

        return res

    def get_date_report(self):
        """Get cmte_id-date combined data

        The report is a list with each unique cmte_id-date records. The data is sorted alphabetical by cmte_id
        and then chronologically by date.

        :return: a list contains unique cmte_id-date grouped data
        """
        report = []
        for cmte_id, date in sorted(self.id_date_dict.keys()):
            total_count, total_amount, small, large, date_str = self.id_date_dict[(cmte_id, date)]
            report.append((cmte_id, date_str, str(get_median(small, large)), str(total_count), str(total_amount)))

        return report

if __name__ == '__main__':
    finder = PoliticalDonorsMedianFinder()

    # open the file as stream and read by line
    with open(sys.argv[1], "r") as f:
        with open(sys.argv[2], "a") as zip_file:
            for line in f:
                parts = line.split(INPUT_DELIMER)

                # parse the data from their positions
                """
                Relevant Data Index FEC
                CMTE_ID:            0
                ZIP_CODE:           10
                TRANSACTION_DT:     13
                TRANSACTION_AMT:    14
                OTHER_ID:           15
                """
                cmte_id = parts[0]
                zip_code = parts[10]
                date_str = parts[13]
                amount = parts[14]
                other_id = parts[15]

                # put data
                zip_row = finder.put_and_get_zip_status(cmte_id, zip_code, date_str, amount, other_id)

                # validate the cmte_id-zip grouped data
                if not zip_row:
                    continue

                # append zip data to file
                zip_file.write(OUTPUT_DELIMER.join(zip_row) + "\n")

    # output the cmte_id-date grouped report
    with open(sys.argv[3], "a") as date_file:
        report = finder.get_date_report()
        for row in report:
            date_file.write(OUTPUT_DELIMER.join(row) + "\n")
