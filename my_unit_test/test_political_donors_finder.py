from src.find_political_donors import *


def test_finder_with_other_id():
    """Because we are only interested in individual contributions, we only want records that have the field,
    OTHER_ID, set to empty. If the OTHER_ID field contains any other value, ignore the entire record and don't
    include it in any calculation"""

    finder = PoliticalDonorsMedianFinder()
    res = finder.put_and_get_zip_status("C00629618", "900017", "01032017", "40", "H6CA34245")
    assert not res
    res = finder.put_and_get_zip_status("C00629618", "900017", "01032017", "40", "")
    assert res


def test_finder_with_invalid_date():
    """If TRANSACTION_DT is an invalid date (e.g., empty, malformed), you should still take the record into
    consideration when outputting the results of medianvals_by_zip.txt but completely ignore the record when
    calculating values for medianvals_by_date.txt"""

    finder = PoliticalDonorsMedianFinder()
    res = finder.put_and_get_zip_status("C00629618", "900017", "", "40", "")
    report = finder.get_date_report()
    assert res
    assert len(report) == 0

    res = finder.put_and_get_zip_status("C00629618", "900017", "2017", "40", "")
    report = finder.get_date_report()
    assert res
    assert len(report) == 0

    res = finder.put_and_get_zip_status("C00629618", "900017", "012017", "40", "")
    report = finder.get_date_report()
    assert res
    assert len(report) == 0

    res = finder.put_and_get_zip_status("C00629618", "900017", "010120171234", "40", "")
    report = finder.get_date_report()
    assert res
    assert len(report) == 0

    res = finder.put_and_get_zip_status("C00629618", "900017", "01322016", "40", "")
    report = finder.get_date_report()
    assert res
    assert len(report) == 0

    res = finder.put_and_get_zip_status("C00629618", "900017", "02292017", "40", "")
    report = finder.get_date_report()
    assert res
    assert len(report) == 0

    res = finder.put_and_get_zip_status("C00629618", "900017", "02292016", "40", "")
    report = finder.get_date_report()
    assert res
    assert len(report) == 1


def test_finder_with_long_zip_code():
    """While the data dictionary has the ZIP_CODE occupying nine characters, for the purposes of the challenge,
    we only consider the first five characters of the field as the zip code"""

    finder = PoliticalDonorsMedianFinder()
    res = finder.put_and_get_zip_status("C00629618", "9000171234", "01032017", "40", "")
    res = finder.put_and_get_zip_status("C00629618", "9000175678", "01032017", "40", "")
    assert int(res[3]) == 2


def test_finder_with_invalid_zip_code():
    """If ZIP_CODE is an invalid zipcode (i.e., empty, fewer than five digits), you should still take the record
    into consideration when outputting the results of medianvals_by_date.txt but completely ignore the record
    when calculating values for medianvals_by_zip.txt"""

    finder = PoliticalDonorsMedianFinder()
    res = finder.put_and_get_zip_status("C00629618", "1234", "01032017", "40", "")
    report = finder.get_date_report()
    assert not res
    assert len(report) == 1
    res = finder.put_and_get_zip_status("C00629618", "", "01032017", "40", "")
    report = finder.get_date_report()
    assert not res
    assert len(report) == 1


def test_finder_with_invalid_cmte_id():
    """If any lines in the input file contains empty cells in the CMTE_ID fields, you should ignore and skip the
     record and not take it into consideration when making any calculations for the output files"""
    finder = PoliticalDonorsMedianFinder()
    res = finder.put_and_get_zip_status("", "90017", "01032017", "40", "")
    report = finder.get_date_report()
    assert not res
    assert len(report) == 0


def test_finder_with_invalid_transaction_amount():
    """If any lines in the input file contains empty cells in the CMTE_ID or TRANSACTION_AMT fields, you should
    ignore and skip the record and not take it into consideration when making any calculations for the output
    files"""

    finder = PoliticalDonorsMedianFinder()
    res = finder.put_and_get_zip_status("C00629618", "90017", "01032017", "", "")
    report = finder.get_date_report()
    assert not res
    assert len(report) == 0

    res = finder.put_and_get_zip_status("C00629618", "90017", "01032017", "a", "")
    report = finder.get_date_report()
    assert not res
    assert len(report) == 0


def test_finder_date_report_order():
    """this second output file should have lines sorted alphabetical by recipient and then chronologically by
    date."""

    finder = PoliticalDonorsMedianFinder()
    finder.put_and_get_zip_status("A2", "90017", "01032017", "100", "")
    finder.put_and_get_zip_status("A2", "90017", "01042017", "100", "")
    finder.put_and_get_zip_status("A2", "90017", "01052017", "100", "")
    finder.put_and_get_zip_status("A2", "90017", "01032016", "100", "")
    finder.put_and_get_zip_status("A1", "90017", "01042017", "100", "")
    finder.put_and_get_zip_status("B1", "90017", "01042017", "100", "")
    finder.put_and_get_zip_status("B2", "90017", "01042017", "100", "")
    finder.put_and_get_zip_status("A2", "90017", "01051999", "100", "")
    finder.put_and_get_zip_status("B2", "90017", "01052017", "100", "")
    finder.put_and_get_zip_status("B2", "90017", "01022017", "100", "")
    report = finder.get_date_report()
    assert all(report[i][0] < report[i + 1][0] or
               report[i][0] == report[i + 1][0] and datetime.datetime.strptime(report[i][1], "%m%d%Y") <=
               datetime.datetime.strptime(report[i + 1][1], "%m%d%Y")
               for i in range(len(report) - 1))


def test_finder_date_report_unique():
    """every line in the medianvals_by_date.txt file should be represented by a unique combination of day and
    recipient"""

    finder = PoliticalDonorsMedianFinder()
    finder.put_and_get_zip_status("A1", "90017", "01042017", "100", "")
    finder.put_and_get_zip_status("A1", "90017", "01042017", "100", "")
    finder.put_and_get_zip_status("B1", "90017", "01042017", "100", "")
    finder.put_and_get_zip_status("B2", "90017", "01042017", "100", "")
    finder.put_and_get_zip_status("A2", "90017", "01051999", "100", "")
    finder.put_and_get_zip_status("B2", "90017", "01052017", "100", "")
    finder.put_and_get_zip_status("A1", "90017", "01042017", "100", "")
    finder.put_and_get_zip_status("A1", "90117", "01042017", "100", "")
    report = finder.get_date_report()
    assert len(report) == 5
    combo = map(lambda t: t[0] + t[1], report)
    assert len(set(combo)) == len(combo)


def test_finder_large_same_input():
    """scales for large amounts of data"""

    finder = PoliticalDonorsMedianFinder()
    for i in range(100000):
        res = finder.put_and_get_zip_status("A1", "90017", "01042017", "100", "")
        assert res[3] == str(i + 1)
    report = finder.get_date_report()
    assert report[0][3] == str(100000)


