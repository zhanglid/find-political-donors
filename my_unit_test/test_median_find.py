from src.find_political_donors import  add_amount, get_median
import random


def test_add_num():
    small = []
    large = []
    for i in range(100):
        add_amount(small, large, i)
    assert len(small) + len(large) == 100


def test_heap_size():
    small = []
    large = []
    for i in range(100):
        add_amount(small, large, i)
        assert len(large) <= len(small) <= len(large) + 1


def test_small_heap_smaller_than_large():
    small = []
    large = []
    for i in range(100):
        add_amount(small, large, i)
        assert not large or large and -small[0] <= large[0]


def test_random_add():
    small = []
    large = []
    size = random.randint(500, 1000)
    for amount in [random.randint(1, 2000) for i in range(size)]:
        add_amount(small, large, amount)
    assert len(small) + len(large) == size


def test_random_heap_size():
    small = []
    large = []
    size = random.randint(500, 1000)
    for amount in [random.randint(1, 1000) for i in range(size)]:
        add_amount(small, large, amount)
        assert len(large) <= len(small) <= len(large) + 1


def test_random_small_heap_smaller_than_large():
    small = []
    large = []
    for i in [random.randint(1, random.randint(1000, 2000)) for i in range(100)]:
        add_amount(small, large, i)
        assert not large or large and -small[0] <= large[0]


def test_median_for_only_one_data():
    small = [-2]
    large = []
    assert get_median(small, large) == 2


def test_median_for_two_data():
    small = []
    large = []
    add_amount(small, large, 1)
    add_amount(small, large, 10)
    assert get_median(small, large) == 6


def test_random_get_median():
    small = []
    large = []
    input = [random.randint(1, 1000) for i in range(100)]
    tmp = []
    for amount in input:
        add_amount(small, large, amount)
        tmp.append(amount)
        tmp = sorted(tmp)
        ans = tmp[len(tmp) / 2]
        if len(tmp) % 2 == 0:
            ans = int(round((tmp[len(tmp) / 2] + tmp[len(tmp) / 2 - 1]) / 2.0))
        assert get_median(small, large) == ans


def test_get_median_same_amount():
    small = []
    large = []
    for i in range(100):
        add_amount(small, large, 5)
        assert get_median(small, large)